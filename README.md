# DEFEAT THE MONSTER:

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.

The application is based on a simple turn-based combat game, in which the player can control the hero to defeat the monster.

## HOW TO RUN
### Preparing the environment 

Commands that must be executed before starting the application.

To download all the dependencies and be able to use the application, run the code below in the main directory:

`npm install` <br />

### JSON Server
To start the JSON server go to the "backend" folder and run the commands:

`npm install` <br />
`npm install -g json-server` <br />

After installing these dependencies, run the command:

`npm start` <br />

## Running the application

To run the application, run the command in the main directory:

`ng serve`  <br />

The application will start running in the URL: `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
