import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from './pages/game/CanDeactivateGuard.service';
import { GameComponent } from './pages/game/game.component';
import { HomeComponent } from './pages/home/home.component';
import { RankingComponent } from './pages/ranking/ranking.component';
import { RulesComponent } from './pages/rules/rules.component';

const routes: Routes = [
  {
   path: '',
   redirectTo: 'home',
   pathMatch: 'full'
  },
  {
   path: 'home',
   component: HomeComponent
  },
  {
   path: 'rules',
   component: RulesComponent
  },
  {
   path: 'ranking',
   component: RankingComponent
  },
  {
   path: 'game',
   component: GameComponent,
   canDeactivate: [CanDeactivateGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
