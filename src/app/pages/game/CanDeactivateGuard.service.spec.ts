/* tslint:disable:no-unused-variable */

import { TestBed, inject } from '@angular/core/testing';
import { CanDeactivateGuard } from './CanDeactivateGuard.service';

describe('Service: CanDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanDeactivateGuard]
    });
  });

  it('should ...', inject([CanDeactivateGuard], (service: CanDeactivateGuard) => {
    expect(service).toBeTruthy();
  }));
});
