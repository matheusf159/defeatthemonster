import { Ranking } from './../ranking/ranking';
import { RankingService } from './../../services/ranking.service';
import { Component } from '@angular/core';
import { InfoLog } from './info-log.interface'
import { Router } from '@angular/router';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent {
  constructor(
    private service: RankingService,
    private  router: Router,
  ) {
    const navigation = this.router.getCurrentNavigation();
    const newPlayer = navigation?.extras.state as { namePlayer: string };
    this.namePlayer = newPlayer?.namePlayer ?? "PlayerName";
  }

  flagScreen = false;

  canDeactivate(): boolean {
    if ( this.flagScreen === false ) {
      return window.confirm('Você tem certeza que quer sair do jogo?');
    }

    return true;
  }

  namePlayer: string;

  playerLife = 100;
  playerDamage = 0;
  playerHeal = 0;
  especialAttackFlag = 0;
  enemyLife = 100;
  especialAttackFlagEnemy = 0;
  enemyDamage = 0;
  stunnedEnemy = 0;
  turns = 0;
  score = 0;

  arrayLogs: InfoLog[] = [{
    description: '',
    damage: '',
    color: ''
  }];



  rankingPlayer: Ranking = {
    name: '',
    data: new Date(),
    score: 0
  };

  getRandomInt( min: number, max: number ): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }

  colorLifePlayer(): string {
    if(this.playerLife >= 100*0.5){
      return 'green';
    } else if ( this.playerLife < 100 * 0.2 ) {
      return 'red';
    } else {
      return 'yellow';
    }
  }

  colorLifeEnemy(): string {
    if( this.enemyLife >= 100 * 0.5 ) {
      return 'green';
    } else if ( this.enemyLife < 100 * 0.2 ) {
      return 'red';
    } else {
      return 'yellow';
    }
  }

  logsFunction(description: string, damage: string, color: string): void {
    const log: InfoLog = {
      description: description,
      damage: damage,
      color: color
    };

    this.arrayLogs.unshift(log);
  }

  enemyStunned(): boolean {
    if(this.stunnedEnemy == 1) {
      this.logsFunction('O monstro ficou atordoado', '(0)', 'stunned-enemy');
      this.stunnedEnemy = 0;

      return true;
    }

    return false;
  }

  EnemyAction(): void {
    if(this.especialAttackFlagEnemy == 3) {
      this.turns = this.turns + 1;

      if(!this.enemyStunned()) {
        this.enemyDamage = this.getRandomInt(8, 17);
        this.playerLife = this.playerLife - this.enemyDamage;
        this.especialAttackFlagEnemy = 0;

        this.logsFunction('O monstro causou dano especial', `(-${this.enemyDamage})`, 'attack-enemy');
      }
    } else {
      if(!this.enemyStunned()) {
        this.turns = this.turns + 1
        this.enemyDamage = this.getRandomInt(6, 13);
        this.playerLife = this.playerLife - this.enemyDamage;
        this.especialAttackFlagEnemy = this.especialAttackFlagEnemy + 1;

        this.logsFunction('O monstro causou dano', `(-${this.enemyDamage})`, 'attack-enemy');
      }
    }

    this.gamerOver();
  }

  gamerOver(): void {
    if(this.playerLife <= 0 || this.enemyLife <= 0) {
      if(this.playerLife <= 0) {
        this.playerLife = 0;
        this.flagScreen = true;
        alert('Você perdeu!!');
        this.router.navigate(['home']);
      }
      else{
        this.enemyLife = 0;

        this.rankingPlayer.name = this.namePlayer;
        this.rankingPlayer.data = new Date();
        this.score = parseFloat(((this.playerLife * 1000) / this.turns).toFixed(2));
        this.rankingPlayer.score = this.score;

        this.service.addRanking(this.rankingPlayer).subscribe();
        this.flagScreen = true;
        alert(`Você ganhou!! Sua pontuação é: ${this.score} pts`);
        this.router.navigate(['ranking']);
      }
    }
  }

  attack(): void {
    this.playerDamage = this.getRandomInt(5, 11);
    this.enemyLife = this.enemyLife - this.playerDamage;
    this.enemyLife = this.enemyLife < 0 ? 0 : this.enemyLife;
    this.turns = this.turns + 1;

    this.logsFunction('Jogador atacou o monstro', `(-${this.playerDamage})`, 'attack-player');

    this.especialAttackFlag = this.especialAttackFlag + 1;
    this.gamerOver();

    if(this.enemyLife !== 0) {
      this.EnemyAction();
    }
  }

  especialAttack(): void {
    this.stunnedEnemy = this.getRandomInt(0, 2);
    this.playerDamage = this.getRandomInt(10, 21);
    this.enemyLife = this.enemyLife - this.playerDamage;
    this.enemyLife = this.enemyLife < 0 ? 0 : this.enemyLife;
    this.turns = this.turns + 1;

    this.logsFunction('Jogador usou o golpe especial', `(-${this.playerDamage})`, 'attack-especial');

    this.especialAttackFlag = 0;
    this.gamerOver();

    if(this.enemyLife !== 0) {
      this.EnemyAction();
    }
  }

  heal(): void {
    this.playerHeal = this.getRandomInt(5, 16);
    this.playerLife = this.playerLife + this.playerHeal;
    this.playerLife = this.playerLife > 100 ? 100 : this.playerLife;
    this.turns = this.turns + 1;

    this.logsFunction('Jogador usou a cura', `(+${this.playerHeal})`, 'heal');

    this.EnemyAction();
    this.especialAttackFlag = this.especialAttackFlag + 1
  }

  quit(): void {
    this.router.navigate(['home']);
  }
}
