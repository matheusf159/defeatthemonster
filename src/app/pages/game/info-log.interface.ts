export interface InfoLog {
  description: string,
  damage: string,
  color: string
}
