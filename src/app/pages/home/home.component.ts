import { Router, NavigationExtras } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(private router: Router) {}

  inputName = false;
  namePlayer = '';

  startGame() {
    this.inputName = true;
  }

  enterName() {
    if(this.namePlayer != '') {
      const navigationExtras: NavigationExtras = {
        state: {namePlayer: this.namePlayer}
      };
      this.router.navigate(['game'], navigationExtras);
    } else {
      alert('Nome obrigatório!!');
    }
  }
}
