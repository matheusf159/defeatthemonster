import { RankingService } from './../../services/ranking.service';
import { Component, Input, OnInit } from '@angular/core';
import { Ranking } from './ranking';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})

export class RankingComponent implements OnInit {
  @Input() rankingPlayer: Ranking[] = [];

  constructor(private service: RankingService) {}

  ngOnInit(): void {
    this.service.list().subscribe((rankingPlayer) => {
        this.rankingPlayer = rankingPlayer;

        this.rankingPlayer.sort((a, b) => b.score - a.score);
    });
  }
}
