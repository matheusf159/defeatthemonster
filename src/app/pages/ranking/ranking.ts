export interface Ranking {
  id?: number,
  name: string,
  data: Date,
  score: number
}
