import { Observable } from 'rxjs';
import { Ranking } from './../pages/ranking/ranking';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  private readonly API = 'http://localhost:3000/rankingPlayer';

  constructor(private http: HttpClient) { }

  list(): Observable<Ranking[]> {
    return this.http.get<Ranking[]>(this.API);
  }

  addRanking(rankingPlayer: Ranking): Observable<Ranking> {
    return this.http.post<Ranking>(this.API, rankingPlayer);
  }
}
